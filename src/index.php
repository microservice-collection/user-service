<?php

require __DIR__ . '/../vendor/autoload.php';

use DI\Bridge\Slim\Bridge;
use DI\ContainerBuilder;
use userservice\core\enums\Storage;
use userservice\core\repositories\RoleRepositoryInterface;
use userservice\core\repositories\UserRepositoryInterface;
use userservice\core\services\AuthenticationServiceInterface;
use userservice\core\services\UserServiceInterface;
use userservice\infrastructure\repositories\ldap\UserRepository as LdapUserRepository;
use userservice\infrastructure\repositories\RoleRepository;
use userservice\infrastructure\repositories\UserRepository;
use userservice\infrastructure\services\AuthenticationService;
use userservice\infrastructure\services\ConfigurationService;
use userservice\infrastructure\services\UserService;
use userservice\webservice\controller\AuthenticationController;
use userservice\webservice\controller\RolesController;
use userservice\webservice\controller\UsersController;
use userservice\webservice\middlewares\AuthenticationMiddleware;
use userservice\webservice\middlewares\BearerMiddleware;
use function DI\get;


/**
 * Mapping of classes to interfaces
 */
$definitions = [
    
    //REPOSITORIES
    //RepositoryInterface::class => get(Repository::class),
    //UserRepositoryInterface::class => get(UserRepository::class),
    RoleRepositoryInterface::class => get(RoleRepository::class),
   
    //SERVICES
    AuthenticationServiceInterface::class => get(AuthenticationService::class),
    UserServiceInterface::class => get(UserService::class)
];

$storage = ConfigurationService::get('storage', true, Storage::toArray());
switch($storage){
    case Storage::DATABASE:
        $definitions[UserRepositoryInterface::class] = get(UserRepository::class);
        break;
    case Storage::LDAP:
         $definitions[UserRepositoryInterface::class] = get(LdapUserRepository::class);
        break;
}

$builder = new ContainerBuilder();
$builder->addDefinitions($definitions);
$container = $builder->build();
$app = Bridge::create($container);

/*
    $app->add(new Cors([
            "origin" => ["*"],
            "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"],
            "headers.allow" => ["Content-Type", "Authorization", "Accept", "Origin"],
            "headers.expose" => [],
            "credentials" => true,
            "cache" => 0,
            "logger" => $container->get(Logger::class),

        ]));
*/


//USERS_CONTROLLER ROUTES
$app->get('/users/{id}', [UsersController::class, 'get']);
$app->get('/users', [UsersController::class, 'getIndex']);
$app->delete('/users/{id}', [UsersController::class, 'delete']);
$app->post('/users', [UsersController::class, 'create']);
$app->put('/users', [UsersController::class, 'update']);

//ROLES_CONTROLLER ROUTES
$app->get('/roles/{id}', [RolesController::class, 'get']);
$app->get('/roles', [RolesController::class, 'getIndex']);
$app->delete('/roles/{id}', [RolesController::class, 'delete']);
$app->put('/roles', [RolesController::class, 'update']);
$app->post('/roles', [RolesController::class, 'create']);

//AUTHENTICATION-CONTROLLER ROUTES
$app->post('/authenticate', [AuthenticationController::class, 'authenticate']);
$app->post('/token/refresh', [AuthenticationController::class, 'refreshToken']);

$app->get('/token', [AuthenticationController::class, 'getToken'])->setName('Generate Token')->add($container->get(AuthenticationMiddleware::class));
$app->get('/token/validate', [AuthenticationController::class, 'validateToken'])->add($container->get(BearerMiddleware::class));
$app->get('/token/refresh', [AuthenticationController::class, 'refreshToken']);
$app->get('/token/request/email', [AuthenticationController::class, 'requestTokenByEmail']);

$app->run();