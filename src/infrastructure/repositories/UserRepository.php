<?php

namespace userservice\infrastructure\repositories;

use userservice\core\repositories\UserRepositoryInterface;
use userservice\infrastructure\repositories\Repository;
use userservice\core\models\User;
use userservice\core\exceptions\UnauthorizedException;

class UserRepository extends Repository implements UserRepositoryInterface {
    
    public function __construct(){
        parent::__construct();
    }
    
    private function getByEmail(string $email): ?User{
        $query = $this->_connection->from(User::TABLE);
        $query = $query->where(User::EMAIL_COLUMN, $email);
        foreach($query as $userData){
            return User::fromData($userData);
        }
    }
   
    public function getIndex(int $page = null, int $size = null, string $email = null): ?array {
        $users = [];
        $query = $this->_connection->from(User::TABLE);
        $query = $email != null ? $query->where(User::EMAIL_COLUMN, $email): $query;
        $query = $this->paginate($query, $page, $size);
        foreach($query as $userData){
            $user = User::fromData($userData);
            $users[] = $user;
        }
        return $users;
    }

    public function authenticate(string $userId, string $password) : User {
        $user = $this->getByEmail($userId);
        $dbPassword = $this->getPassword($user->getId());
        if(empty($user) || !isset($user)){
            throw new NotFoundException("The user ".$email." doesn't exist");
        }
        if(empty($dbPassword) || empty($password)){
            throw new UnauthorizedException("The user doesn't have a password");
        }
        if($dbPassword != $password){
            throw new UnauthorizedException("The password is wrong");
        }
        return $user;
    }

    
    public function insert(User $user) {
        $userData = $user->toData();
        $result = $this->_connection->insertInto(User::TABLE, $userData)->execute();
        if(!$result){
            throw new Exception("Inserting the user an unexpected error occured");
        }
    }

    public function delete(string $userId) {
        $this->_connection->deleteFrom(User::TABLE, $user->getId())->execute();
    }

    public function update(User $user) {
        $existingUser = $this->get($user->getId());
        if(empty($existingUser)){
            throw new NotFoundException("The user ".$user->getId()." doesn't exist.");
        }
        $this->_connection->update(User::TABLE, $user->toData(), $user->getId())->execute();
    }

    /**
     * 
     * @param string $userId
     * @return User|null
     */
    public function get(string $userId): ?User {
        $query = $this->_connection->from(User::TABLE);
        $query = $query->where(User::ID_COLUMN, $userId);
        $data = $query->fetch();
        if(empty($data)){
            return null;
        }
        return User::fromData($data);
    }

    public function getPassword(string $userId): string {
        $query = $this->_connection->from(User::TABLE);
        $query = $query->where(User::ID_COLUMN, $userId);
        $query = $query->select(User::PASSWORD_COLUMN);
        $data = $query->fetch();
        if(!$data) {
            return null;
        }
        return $data[User::PASSWORD_COLUMN];
    }

    public function setPassword(string $userId, string $password) {
        $existingUser = $this->get($userId);
        if(empty($existingUser)){
            throw new NotFoundException("The user ".$user->getId()." doesn't exist.");
        }
        $this->_connection->update(User::Table, $password, $userId)->execute();
    }

}