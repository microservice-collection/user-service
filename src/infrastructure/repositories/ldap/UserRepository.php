<?php

namespace userservice\infrastructure\repositories\ldap;

use userservice\core\repositories\UserRepositoryInterface;
use userservice\core\services\ConfigurationServiceInterface;

class UserRepository implements UserRepositoryInterface{
    
    //LDAP CONFIGS
    private const LDAP_DOMAIN_CONFIG_KEY ="ldap_domain";
    private const LDAP_ADDRESS_CONFIG_KEY = "ldap_address";
    private const LDAP_PORT_CONFIG_KEY ="ldap_port";
    
    
    /**
     *
     * @var ConfigurationServiceInterface
     */
    private $_configuration;
    protected $_ldap;
    protected $_ldapDomain;
    
    public function __construct(ConfigurationServiceInterface $configuration){
        $this->_configuration = $configuration;
        $this->_ldapDomain = $this->_configuration->get(self::LDAP_DOMAIN_CONFIG_KEY);
        $ldapAddress = $this->_configuration->get(self::LDAP_ADDRESS_CONFIG_KEY);
        $ldapPort = $this->_connection->get(self::LDAP_PORT_CONFIG_KEY);
        $this->_ldap = ldap_connect($ldapAddress, $ldapPort);
        if(!$this->_ldap){
            throw new Exception("LDAP-Connection failed");
        }
        ldap_set_option($this->_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->_connection, LDAP_OPT_REFERRALS, 0);
    }
    
    public function authenticate(string $userId, string $password): \userservice\core\models\User {
        $bind = ldap_bind($this->_ldap, $this->_ldapDomain."\\".$userId, $password);
        if(!$bind){
            throw new UnauthorizedException("The user credentails are not valid");
        }
        $user = $this->get($userId);
    }

    public function get(string $userId): ?\userservice\core\models\User {
        throw new Exception("Not implemented yet.");
    }

    public function getIndex(int $page = null, int $size = null, string $email = null): ?array {
        throw new Exception("Not implemented yet");
    }


    public function delete(string $userId) {
        throw new Exception("Not supported for ldap. User administration needs to be done in Active Directory.");
    }

    public function insert(\userservice\core\models\User $user) {
        throw new Exception("Not supported for ldap. User administration needs to be done in Active Directory.");
    }

    public function update(\userservice\core\models\User $user) {
        throw new Exception("Not supported for ldap. User administration needs to be done in Active Directory.");
    }

}