<?php

namespace userservice\infrastructure\repositories;

use Ramsey\Uuid\UuidInterface;
use userservice\core\models\Role;
use userservice\core\models\UserRole;
use userservice\core\repositories\RoleRepositoryInterface;

class RoleRepository extends Repository implements RoleRepositoryInterface{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function delete(UuidInterface $id): void {
         $this->_connection->deleteFrom(Role::TABLE, $id->toString())->execute();
    }

    public function get(UuidInterface $id): ?Role {
        $query = $this->_connection->from(Role::TABLE);
        $query = $query->where(Role::ID_COLUMN, $id);
        $data = $query->fetch();
        if(!$data){
            return null;
        }
        return Role::fromData($data);
    }

    public function getIndex(int $page = null, int $size = null, UuidInterface $userRoleId = null, $userId = null): ?array {
        $roles =[];
        $query = $this->_connection->from(Role::TABLE);
        $query = $query->leftJoin(UserRole::TABLE ." ON ".UserRole::TABLE.".".UserRole::ROLE_ID_COLUMN." = ".Role::TABLE.".". Role::ID_COLUMN);
        $query = $userRoleId ? $query->where(UserRole::TABLE.'.'.UserRole::ROLE_ID_COLUMN,$userRoleId->toString()): $query;
        $query = $userId ? $query->where(UserRole::TABLE.'.'.UserRole::USER_ID_COLUMN, $userId) : $query;
        $query = $this->paginate($query, $page, $size);
        $data = $query->fetchAll();
        if(!$data){
            return null;
        }
        foreach($query as $role){
            $role = Role::fromData($role);
            $roles[] = $role;
        }
        return $roles;
    }

    public function insert(Role $role): void {
        $role->validate(); //Throws ValidationException
        $result = $this->_connection->insertInto(Role::TABLE, $role->toData())->execute();
        if(!$result){
            throw new Exception("Inserting the role an unexpected error occured");
        }
    }

    public function update(Role $role): void {
        $existingRole = $this->get($role->getId());
        if(empty($existingRole)){
            throw new NotFoundException("The role ". $role->getId()->toString(). " doesn't exist");
        }
        $this->_connection->update(Role::TABLE, $role->toData(), $role->getId())->execute();
    }

}