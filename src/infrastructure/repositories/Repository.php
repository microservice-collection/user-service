<?php

namespace userservice\infrastructure\repositories;

use userservice\core\repositories\RepositoryInterface;
use userservice\infrastructure\services\ConfigurationService;
use \Envms\FluentPDO\Query;

abstract class Repository extends \PDO implements RepositoryInterface{
    private const STORAGE_CONFIG_KEY ="storage";
    private const AUTHENTICATION_CONFIG_KEY ="authorization";
    
    //DATABASE CONFIGS
    private const DATABASE_DSN_CONFIG_KEY="database:dsn";
    private const DATABASE_USERNAME_CONFIG_KEY = "database:username";
    private const DATABASE_PASSWORD_CONFIG_KEY ="database:password";
    
    /**
     *
     * @var FluentPdo Query
     */
    protected $_connection;
 
    /**
     * 
     */
    public function __construct(){
        $dsn = ConfigurationService::get(self::DATABASE_DSN_CONFIG_KEY);
        $username = ConfigurationService::get(self::DATABASE_USERNAME_CONFIG_KEY);
        $passwd = ConfigurationService::get(self::DATABASE_PASSWORD_CONFIG_KEY);
        $options = [];
        parent::__construct($dsn, $username, $passwd, $options);
        $this->_connection = new Query($this);
        /*$this->_connection->debug = function($BaseQuery) {
            echo "query: " . $BaseQuery->getQuery(false) . "<br/>";
            echo "parameters: " . implode(', ', $BaseQuery->getParameters()) . "<br/>";
            echo "rowCount: " . $BaseQuery->getResult()->rowCount() . "<br/>";  
        };*/
    }
    public function __destruct() {

    }
    
    /**
     * 
     * @param type $query
     * @param int $page
     * @param int $size
     * @return type
     */
    protected function paginate($query, int $page = null, int $size = null){
        if($page && $size){
            $query = $query->offset(($page-1)*$size)->limit($size);
        }elseif($size){
            $query = $query->limit($size);
        }
        return $query;
    }
    
    
    
}