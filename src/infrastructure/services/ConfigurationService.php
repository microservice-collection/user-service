<?php

namespace userservice\infrastructure\services;

use userservice\core\exceptions\ConfigurationException;

/**
 * TODO extend by default value
 */
class ConfigurationService{
    
    /**
     * @var string
     */
    private const CONFIG_PATH = "../config/config.json";
    /**
     *
     * @var array
     */
    private $configuration;
    
    public function __construct() {
        $file = file_get_contents(self::CONFIG_PATH);
        if(!$file){
            throw new FileNotFoundException("No configuration under ".self::CONFIG_PATH.' found');
        }
        $this->configuration = json_decode($file, true);
    }
    
    private static function getConfiguration() : array{
        $file = file_get_contents(self::CONFIG_PATH);
        if(!$file){
            throw new FileNotFoundException("No configuration under ".self::CONFIG_PATH.' found');
        }
        return  json_decode($file, true);
    }
    
    public static function get(string $key, bool $required = true, array $contains = null): ?string {
        //TODO: Caching
        $configuration = ConfigurationService::getConfiguration();
        
        $value =& $configuration;
        foreach(explode(':', $key) as $keyElement) {
            $value =& $value[$keyElement];
        }
        
        if(empty($value) && $required){
            throw new ConfigurationException("No configuration found for key '".$key."'");
        }
        if($contains != null && !in_array($value, $contains)){
            throw new ConfigurationException("'".$value."' in '".$key."' doesn't match to allowed list: ".implode(", ", $contains));
        }
        return $value;
    }

    public static function getBool(string $key, bool $required = true, array $contains = null): ?bool {
        $value = get($key, $required, $contains);
        $bool = boolval($value);
        return $bool;
    }

    public static function getDateTime(string $key, bool $required = true, array $contains = null): ?\DateTime {
        $value = ConfigurationService::get($key, $required, $contains);
        $datetime = \DateTime::createFromFormat($value, 'Y-m-d H:i:s');
        return $datetime;
    }

    public static function getInt(string $key, bool $required = true, array $contains = null): ?int {
        $value = ConfigurationService::get($key, $required, $contains);
        $int = intval($value);
        return $int;
    }

    public static function getFileContent(string $key, bool $required = true) {
        $path = ConfigurationService::get($key, $required);
        if(empty($path)){
            return null;
        }
        if(!file_exists($path)){
            throw new ConfigurationException("The file configured in ".$privateKeyPath." doesn't exist");
        }
        $content =  file_get_contents($path);
        return $content;
    }

}