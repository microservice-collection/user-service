<?php
namespace userservice\infrastructure\services;

use userservice\core\exceptions\NotFoundException;
use userservice\core\exceptions\ValidationException;
use userservice\core\models\User;
use userservice\core\repositories\UserRepositoryInterface;
use userservice\core\services\UserServiceInterface;

/**
 * 
 */
class UserService implements UserServiceInterface{
    /**
     *
     * @var UserRepositoryInterface 
     */
    private $_userRepository;
    
    /**
     * 
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
            UserRepositoryInterface $userRepository) {
        $this->_userRepository = $userRepository;
    }
    
    public function get(string $key): User {
        $user = $this->_userRepository->get($key);
        if($user == null){
            throw new NotFoundException("User could not be found");
        }
        return $user;
    }

    public function getIndex(int $page = null, int $size = null, string $email = null): array {
        $users = $this->_userRepository->getIndex($page, $size, $email);
        if($users == null || count($users) == 0){
            throw new NotFoundException("No users found");
        }
        return $users;
    }
    
    public function authenticate(string $username, string $password): User {
        return $this->_userRepository->authenticate($username, $password);
    }
    
    public function create(User $user): User{
        $user->validate(); //throws ValidationException
        $this->validate($user);
        $this->_userRepository->insert($user);
        //TODO load created user with new id
        return $user;
    }
    
    public function delete(string $userId) {
        $user = $this->get($userId); //throws NotFoundException
        $this->_userRepository->delete($userId);
    }

    public function update(User $user) {
        $user->validate(); //throws ValidationException
        $this->validate($user); //throws ValidationException
        $this->_userRepository->update($user);
    }
    
    /**
     * Validates user dependencies
     * @param User $user
     * @throws ValidationException
     */
    private function validate(User $user){
        //TODO
    }

}
