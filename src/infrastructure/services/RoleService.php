<?php

namespace userservice\infrastructure\services;

use Ramsey\Uuid\UuidInterface;
use userservice\core\models\Role;
use userservice\core\services\RoleServiceInterface;


class RoleService implements RoleServiceInterface{
    
    public function create(Role $role): Role {
        
    }

    public function delete(UuidInterface $id): void {
        
    }

    public function get(UuidInterface $id): Role {
        
    }

    public function getIndex(int $page = null, int $size = null, UuidInterface $userRoleId = null, string $userId = null): array {
        
    }

    public function update(Role $role): Role {
        
    }

}