<?php

namespace userservice\infrastructure\services;

use DateInterval;
use DateTime;
use Firebase\JWT\JWT;
use TheSeer\Tokenizer\Exception;
use userservice\core\exceptions\UnauthorizedException;
use userservice\core\repositories\RoleRepositoryInterface;
use userservice\core\repositories\UserRepositoryInterface;
use userservice\core\services\AuthenticationServiceInterface;
use userservice\core\services\UserServiceInterface;

class AuthenticationService implements AuthenticationServiceInterface{
    private const PRIVATE_KEY_CONFIG_KEY = "authentication:privateKeyPath";
    private const PUBLIC_KEY_CONFIG_KEY = "authentication:publicKeyPath";
    private const JWT_ISSER_CONFIG_KEY = "authentication:issuer";
    private const JWT_AUDIENCE_CONFIG_KEY = "authentication:audience";
    private const JWT_EXPIRES_IN_DAYS_CONFIG_KEY = "autentication:expiresInDays";
    private const JWT_NOT_BEFORE_IN_HOURS_CONFIG_KEY = "authentication:notBeforeInHours";
    private const JWT_SUBJECT_CONFIG_KEY = "authentication:subject";
    public const AUTHORIZATION_VARIANT_CONFIG_KEY = 'authenticationVariant';
    
    /**
     *
     * @var UserRepositoryInterface
     */
    private $_userService;
    private $_roleRepository;
    
    public function __construct(UserServiceInterface $userService, RoleRepositoryInterface $roleRepository) {
        $this->_userService = $userService;
        $this->_roleRepository = $roleRepository;
    }
    
    public function authenticate(string $username, string $password): string{
        if(empty($username) || empty($password)){
            throw new UnauthorizedException();
        }
        $user = $this->_userService->authenticate($username, $password);
        //TODO get roles
        return $this->generateToken($user);
    }
   
    public function generateToken(string $userId): string{
        $user = $this->_userService->get($userId); //Throws not found exception
        //TODO get Roles
        $roles = $this->_roleRepository->getIndex(null, null, null, $userId);
        $roleData =[];
        foreach($roles as $role){
            $roleData[$role->getId()] = $role->getName();
        }
        $privateKey = ConfigurationService::getFileContent(self::PRIVATE_KEY_CONFIG_KEY);  
        $now = new DateTime();
        
        $payload = [
            "iss" => ConfigurationService::get(self::JWT_ISSER_CONFIG_KEY, false),
            "aud" => ConfigurationService::get(self::JWT_ISSER_CONFIG_KEY, false),
            "iat" => $now->getTimestamp(),
            "nbf" => $this->getNotBefore(),
            "userId" => $user->getId(),
            "roles" => $roleData
        ];
        $jwt = JWT::encode($payload, $privateKey, 'RS256');
        return $jwt;
    }
    
    
    public function validateToken(string $token) : object{
        try{
            $publicKey = ConfigurationService::getFileContent(self::PUBLIC_KEY_CONFIG_KEY);
            $tokenInformation = JWT::decode($token, $publicKey, array('RS256'));
            return $tokenInformation;
        }catch(Exception $ex){
            throw new UnauthorizedException("The token couldn't be validated.");
        }
    }
    
    public function refreshToken(string $token): string{
        $tokenInformation = $this->validateToken($token);
        return $this->generateToken($tokenInformation["userId"], $tokenInformation["roles"]);
    }
   
    private function getNotBefore() : ?string{
        $notBeforeHours = ConfigurationService::getInt(self::JWT_NOT_BEFORE_IN_HOURS_CONFIG_KEY, false);
        $notBefore = new DateTime();
        if(!empty($notBeforeDays)){
            $notBefore = $notBefore->add(new DateInterval('P'.$notBeforeHours.'D'));
        }
        return $notBefore->getTimestamp();
    }
}