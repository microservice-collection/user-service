<?php

namespace userservice\infrastructure\contexts;

use PDO;
use userservice\core\services\ConfigurationServiceInterface;

/**
 * 
 */
class Context extends PDO{
    private const DSN_CONFIG_KEY="dsn";
    private const USERNAME_CONFIG_KEY = "username";
    private const PASSWORD_CONFIG_KEY ="passwd";
    protected $hasActiveTransaction = false;
    
    public function __construct(ConfigurationServiceInterface $configuration){
        $dsn = $configuration->get(self::DSN_CONFIG_KEY);
        $username = $configuration->get(self::USERNAME_CONFIG_KEY);
        $passwd = $configuration->get(self::PASSWORD_CONFIG_KEY);
        $options = [];
        parent::__construct($dsn, $username, $passwd, $options);
    }

    function beginTransaction () {
        if ($this->hasActiveTransaction ) {
            return false;
        }
        $this->hasActiveTransaction = parent::beginTransaction ();
        return $this->hasActiveTransaction;
   }

   function commit () {
      parent::commit ();
      $this->hasActiveTransaction = false;
   }

   function rollback () {
      parent::rollback ();
      $this->hasActiveTransaction = false;
   }
}