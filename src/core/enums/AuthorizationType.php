<?php

namespace userservice\core\enums;


class AuthorizationType{
    const BEARER = 'Bearer';
    const BASIC = 'Basic';
    const API_KEY = 'Api-Key';
    
   public static function toArray(): array{
       return array(self::BASIC, self::BEARER, self::API_KEY);
   }
}
