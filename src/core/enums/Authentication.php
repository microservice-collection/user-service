<?php

namespace userservice\core\enums;


abstract class Authentication{
    public const DATABASE = 'database';
    public const LDAP = 'ldap';
    
    public static function toArray(): array{
       return array(self::DATABASE, self::LDAP);
   }
}
