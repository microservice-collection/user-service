<?php

namespace userservice\core\exceptions;


class ValidationException extends \Exception{
    public $errors = [];
    
    /**
     * 
     * @param string $message
     * @param array $errors
     * @param int $code
     * @param \Throwable $previous
     * @return \Exception
     */
    public function __construct(string $message = "", array $errors=[], int $code = 0, \Throwable $previous = NULL): \Exception {
        $this->errors = $errors;
        $message += ": ";
        foreach($errors as $error){
            $message += $error . " | "; 
        }
         parent::__construct($message, $code, $previous);
    }
}