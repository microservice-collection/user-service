<?php

namespace userservice\core\repositories;
use userservice\core\models\User;
use userservice\core\exceptions\NotFoundException;
use userservice\core\exceptions\DuplicateException;


interface UserRepositoryInterface extends RepositoryInterface{
    /**
     * 
     * @param string $userId
     * @return User|null
     */
    public function get(string $userId) : ?User;
    /**
     * 
     * @param int $page
     * @param int $size
     * @param string $email
     * @return array|null
     */
    public function getIndex(int $page = null, int $size = null, string $email = null) : ?array;
    
    public function getPassword(string $userId) : string;
    public function setPassword(string $userId, string $password);
    /**
     * 
     * @param string $userId
     * @param string $password
     * @return User
     */
    public function authenticate(string $userId, string $password) : User;
    /**
     * 
     * @param User $user
     * @throws DuplicateException
     */
    public function insert(User $user);
    /**
     * 
     * @param User $user
     * @throws NotFoundException
     */
    public function update(User $user);
    /**
     * 
     * @param string $userId
     * @throws NotFoundException
     */
    public function delete(string $userId);
}