<?php

namespace userservice\core\repositories;

use userservice\core\models\Role;
use Ramsey\Uuid\UuidInterface;
use userservice\core\exceptions\DuplicateException;
use userservice\core\exceptions\NotFoundException;

interface RoleRepositoryInterface extends RepositoryInterface{
    
    /**
     * 
     * @param UuidInterface $id
     * @return Role|null
     */
    public function get(UuidInterface $id) : ?Role;
    /**
     * 
     * @param int $page
     * @param int $size
     * @return array|null
     */
    public function getIndex(int $page = null, int $size = null, UuidInterface $userRoleId = null , string $userId = null) : ?array;
    /**
     * 
     * @param Role $role
     * @return void
     */
    public function insert(Role $role): void;
    /**
     * 
     * @param Role $role
     * @return void
     * @throws NotFoundException
     */
    public function update(Role $role): void;
    /**
     * 
     * @param UuidInterface $id
     * @return void
     */
    public function delete(UuidInterface $id): void;
}
