<?php

namespace userservice\core\models;

use userservice\core\exceptions\ValidationException;

class Role extends Model{
    public const TABLE = "role";
    public const NAME_COLUMN="name";
    
    
    /**
     *
     * @var string
     */
    private $name;
    
    
    public function getName(): string{
        return $this->name;
    }
    
    public function setName(string $name){
        $this->name = $name;
    }
    
    public function toData(): array {
        return [
            self::NAME_COLUMN => $this->name
        ];
    }

    public function validate() {
        $errors = [];
        $errors[] = $this->validateName();
        if(count($errors) > 0){
            throw new ValidationException("Role is not valid", $errors);
        }
    }

    public static function fromData(array $data): object {
        $role = new Role();
        $role->setName($data[self::NAME_COLUMN]);
        return $role;
    }
    
    private function validateName(){
        if(empty($this->name)){
            return "Role name must be set";
        }
        if(strlen($this->name) > 100){
            return "Role name cannot have more than 100 characters";
        }
    }

}