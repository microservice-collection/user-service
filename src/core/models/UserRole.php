<?php

namespace userservice\core\models;

class UserRole extends Model{
    public const TABLE ='user_role';
    public const ID_COLUMN = 'id';
    public const USER_ID_COLUMN = 'user_id';
    public const ROLE_ID_COLUMN = 'role_id';
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function toData(): array {
        
    }

    public function validate() {
        
    }

    public static function fromData(array $data): object {
        
    }

}