<?php

namespace userservice\core\models;

use Ramsey\Uuid\Uuid;

abstract class Model{
    public const ID_COLUMN = 'id';
    public const CREATED_AT_COLUMN = "created_at";
    /**
     *
     * @var UuidInterface | ldap-id
     */
    protected $id;
    
    /**
     *
     * @var \DateTime
     */
    protected $createdAt;
    
    /**
     * 
     */
    public function __construct() {
        $this->id = Uuid::uuid4();
    }
    
    public function getId(): string{
        return $this->id;
    }
    
    public function setId(string $id){
        $this->id = $id;
    }
    
    public function getCreatedAt():\DateTime{
        return $this->createdAt;
    }
    
    public function setCreatedAt(\DateTime $createdAt){
        $this->createdAt = $createdAt;
    }
    
    public static abstract function fromData(array $data) : object;
    public abstract function toData() : array;
    /**
     * @throws ValidationException
     */
    public abstract function validate();
    //public abstract function validate();
}