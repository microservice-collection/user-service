<?php

namespace userservice\core\models;

use DateTime;
use userservice\core\exceptions\ValidationException;

class User extends Model{
    public const TABLE = "user";
    public const ID_COLUMN = "id";
    public const NAME_COLUMN="name";
    public const EMAIL_COLUMN="email";
    public const PASSWORD_COLUMN = "password";
    public const BIRTHDAY_COLUMN = "birthday";
    /**
     *
     * @var string
     */
    private $name;
    
    /**
     *
     * @var string
     */
    public $email;
    
    
    /**
     *
     * @var datetime
     */
    public $birthday;
    
    /**
     *
     * @var string
     */
    private $password;
    
    /**
     *
     * @var array userservice\core\models\Role
     */
    private $roles;
    
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getId() : string{
        return $this->id;
    }
    
    public function getName(): string{
        return $this->name;
    }
    public function setName(string $name){
        $this->name = $name;
    }
    
    public function getEmail():string{
        return $this->email;
    }
    
    public function setEmail(string $email){
        $this->email = $email;
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function setPassword($password){
        $this->password = $password;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getBirthday(): \DateTime{
        return $this->birthday;
    }
    
    /**
     * 
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday){
        $this->birthday = $birthday;
    }
    
    public static function fromData(array $data): object {
        $user = new User();
        if(key_exists(self::ID_COLUMN, $data)){
            $user->id = $data[self::ID_COLUMN];
        }
        $user->name = $data[self::NAME_COLUMN];
        $user->email = $data[self::EMAIL_COLUMN];
        //$user->setPassword($data[self::PASSWORD_COLUMN]);
        if(key_exists(self::BIRTHDAY_COLUMN, $data)){
            $user->setBirthday(DateTime::createFromFormat('Y-m-d H:i:s', $data[self::BIRTHDAY_COLUMN]));
        }
        if(key_exists(self::CREATED_AT_COLUMN, $data)){
            $user->setCreatedAt(DateTime::createFromFormat('Y-m-d H:i:s',$data[self::CREATED_AT_COLUMN]));
        }
        return $user;
    }

    public function toData(): array {
        return [
            self::ID_COLUMN => $this->id,
            self::EMAIL_COLUMN => $this->email,
            self::BIRTHDAY_COLUMN => $this->birthday->format('Y-m-d H:i:s'),
            self::NAME_COLUMN => $this->name,
            //self::PASSWORD_COLUMN => 'password1'
        ];
    }

    /**
     * 
     * @throws ValidationException
     */
    public function validate() {
        //Email Validation
        $errors = [];
        $errors[] =$this->validateEmail();
        $errors[] = $this->validatePassword();
        
        if(count($errors)>0){
            throw new ValidationException("The user is not valid", $errors);
        }
    }
    
    private function validateEmail() : string{
        if(empty($this->email)){
            return "The email of user is required";
        }
        $emailRegex = '/^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/';
        $match = preg_match($emailRegex, $this->email);
        if(!$match){
            return  "The email address has wrong format";
        }
        if(strlen($this->email) > 100){
            return  "The email cannot have more than 100 characters";
        }
    }
    
    /**
     * 
     * @return array
     */
    private function validatePassword(): array{
        $errors = [];
        if(!empty($this->password) && strlen($this->password) > 50){
            $errors[] = "The password cannot have more than 50 characters";
        }
        return $errors;
    }

}

