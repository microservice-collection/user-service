<?php
namespace userservice\core\services;

use Ramsey\Uuid\UuidInterface;
use userservice\core\models\User;
use userservice\core\exceptions\NotFoundException;

interface UserServiceInterface{
    /**
     * 
     * @param string $key
     * @return User
     * @throws NotFoundException
     */
    public function get(string $key): User;
    /**
     * 
     * @param int $page
     * @param int $size
     * @param string $email
     * @return array
     * @throws NotFoundException
     */
    public function getIndex(int $page = null, int $size = null, string $email = null): array;
    
    /**
     * 
     * @param string $username
     * @param string $password
     * @return User
     */
    public function authenticate(string $username, string $password) : User;
    
    /**
     * creates and returns user
     * @param User $user
     * @return User
     */
    public function create(User $user): User;
    
    public function update(User $user);
    public function delete(string $userId);
}
