<?php

namespace userservice\core\services;

use Ramsey\Uuid\UuidInterface;
use userservice\core\models\Role;
use userservice\core\exceptions\NotFoundException;
use userservice\core\exceptions\ValidationException;
use userservice\core\exceptions\DuplicateException;

interface RoleServiceInterface {
    /**
     * 
     * @param UuidInterface $id
     * @return Role|null
     * @throws NotFoundException
     */
    public function get(UuidInterface $id) : Role;
    
    /**
     * 
     * @param int $page
     * @param int $size
     * @param UuidInterface $userRoleId
     * @param string $userId
     * @return array
     * @throws NotFoundException
     */
    public function getIndex(int $page = null, int $size = null, UuidInterface $userRoleId = null , string $userId = null): array;
    
    /**
     * 
     * @param Role $role
     * @return Role
     * @throws ValidationException
     * @throws DuplicateException
     */
    public function create(Role $role): Role;
    
    /**
     * 
     * @param Role $role
     * @return Role
     * @throws ValidationException
     * @throws NotFoundException
     */
    public function update(Role $role): Role;
    
    /**
     * 
     * @param UuidInterface $id
     * @return void
     * @throws NotFoundException
     */
    public function delete(UuidInterface $id): void;
}