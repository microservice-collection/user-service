<?php
namespace userservice\core\services;

use userservice\core\models\User;
use userservice\core\exceptions\NotFoundException;

interface AuthenticationServiceInterface{
    /**
     * generates new jwt token.
     * @param \userservice\core\services\User $user
     * @return string token
     */
    public function generateToken(string $userId): string;
    /**
     * validates given token and refreshs it if valid
     * @param string $token
     * @return string
     */
    public function refreshToken(string $token): string;
    
    /**
     * 
     * @param string $token
     * @return array
     * @throws \userservice\core\exceptions\UnauthorizedException
     */
    public function validateToken(string $token) : object;
    
    /**
     * authenticates user credentials
     * @param string $username
     * @param string $password
     * @returns string generated jwt token
     * @throws NotFoundException User doesn't exist
     */
    public function authenticate(string $username, string $password) : string;
}