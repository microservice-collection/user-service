<?php

namespace userservice\webservice\controller;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid as Uuid;
use TheSeer\Tokenizer\Exception;
use userservice\core\exceptions\DuplicateException;
use userservice\core\exceptions\NotFoundException;
use userservice\core\exceptions\ValidationException;
use userservice\core\models\Role;
use userservice\core\services\RoleServiceInterface;


class RolesController{
   
    /**
     *
     * @var userservice\core\services\RoleServiceinterface
     */
    private $_roleService;
    
    /**
     * 
     * @param RoleServiceInterface $roleService
     */
    public function __construct(RoleServiceInterface $roleService) {
        $this->_roleService = $roleService;
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function create(ResponseInterface $response, RequestInterface $request): ResponseInterface{
        try{
            $data = $request->getParsedBody();
            $role = Role::fromData($data);
            $createdRole = $this->_roleService->create($role);
            return $response->withJson((object)$createdRole->toData(), 200);
        }catch(ValidationException $e){
            return $response->withStatus(400);
        } catch(DuplicateException $e){
            return $response->withStatus(400);
        }
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function get(ResponseInterface $response, RequestInterface $request): ResponseInterface{
        try{
             $roleId = $request->getAttributes()['__route__']->getArguments()['id'];
             $id = Uuid::fromString($roleId);
             $role = $this->_roleService->get($id);
             return $response->withJson((object)$role->toData(),200);
        }catch(NotFoundException $e){
            return $response->withStatus(204);
        }
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function getIndex(ResponseInterface $response, RequestInterface $request):ResponseInterface{
        try{
            $roles = $this->_roleService->getIndex();
            return $response->withJson($roles, 200);
        }catch(NotFoundException $e){
            return $response->withStatus(204);
        } 
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function update(ResponseInterface $response, RequestInterface $request):ResponseInterface{
        try{
            $data = $request->getParsedBody();
            $role = Role::fromData($data);
            $updatedRole = $this->_roleService->update($role);
            return $response->withJson((object)$updatedRole->toData(),200);
        }catch(NotFoundException $e){
            return $response->withStatus(400);
        } catch(ValidationException $e){
            return $response->withStatus(400);
        }
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
   public function delete(ResponseInterface $response, RequestInterface $request) : ResponseInterface{
       try{
             $roleId = $request->getAttributes()['__route__']->getArguments()['id'];
             $id = Uuid::fromString($roleId);
             $this->_roleService->delete($id);
             return $response->withStatus(200);
       }catch(NotFoundException $e){
           return $response->withStatus(400);
       } 
       catch (Exception $ex) {
           return $response->withStatus(500);
       }
   }
}