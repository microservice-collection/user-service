<?php

namespace userservice\webservice\controller;


use userservice\core\services\UserServiceInterface;
use userservice\core\exceptions\NotFoundException;
use userservice\core\exceptions\ValidationException;
use \Psr\Http\Message\RequestInterface;
use \Psr\Http\Message\ResponseInterface;
use userservice\core\models\User;

use Ramsey\Uuid\Uuid;
use \Exception;

class UsersController{
    
    
    /**
     *
     * @var UserServiceInterface
     */
    private $_userService;
    
    public function __construct(UserServiceInterface $userService) {
        $this->_userService = $userService;
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function get(ResponseInterface $response, RequestInterface $request){
        try{
        $userId = $request->getAttributes()['__route__']->getArguments()['id'];
        $user = $this->_userService->get($userId);
        return $response->withJson($user->toData(), 200);
        }catch(NotFoundException $e){
            return $response->withStatus(204);
        }catch(Exception $e){
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function getIndex(ResponseInterface $response, RequestInterface $request){
        try{
            $users = $this->_userService->getIndex();
            return $response->withJson($users, 200);
        }catch(NotFoundException $e){
            return $response->withStatus(204);
        }
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function create(ResponseInterface $response, RequestInterface $request) :ResponseInterface{
        try{
            $data = $request->getParsedBody();
            $user = User::fromData($data);
            $user = $this->_userService->create($user);
            return $response->withJson($user->toObject(), 200);
        }catch(ValidationException $e){
            return $response->withStatus(400);
        } 
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function update(ResponseInterface $response, RequestInterface $request):ResponseInterface{
        try{
            $requestUser = $request->getBody();
            $user = $this->_userService->update($user);
            return $response->withJson($user->toObject(), 200);
        }catch(ValidationException $ex){
            return $response->withStatus(400);
        } 
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    /**
     * 
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function delete(ResponseInterface $response, RequestInterface $request):ResponseInterface{
        try{
            $userId = $request->getAttributes()['__route__']->getArguments()['id'];
            $this->_userService->delete($userId);
            return $response->withStatus(200);
        }catch(NotFoundException $e){
            return $response->withStatus(400);
        } 
        catch (Exception $ex) {
            return $response->withStatus(500);
        }
    }
    
    
}