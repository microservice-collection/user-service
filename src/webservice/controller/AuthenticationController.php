<?php

namespace userservice\webservice\controller;

use userservice\core\services\AuthenticationServiceInterface;
use \Psr\Http\Message\RequestInterface;
use \Psr\Http\Message\ResponseInterface;
use userservice\core\exceptions\UnauthorizedException;
use \userservice\core\enums\AuthorizationType;
use userservice\webservice\middlewares\AuthenticationMiddleware;
use userservice\webservice\middlewares\BearerMiddleware;

/**
 * Handles routes for authentication
 */
class AuthenticationController{
    /**
     *
     * @var AuthenticationServiceInterface 
     */
    private $_authenticationService;
    
    /**
     * 
     * @param AuthenticationServiceInterface $authenticationService
     */
    public function __construct(
            AuthenticationServiceInterface $authenticationService) {
        $this->_authenticationService = $authenticationService;
    }
    
    /**
     * Authenticates user and generates jwt token
     * @param \userservice\webservice\controller\ResponseInterface $response
     * @param \userservice\webservice\controller\RequestInterface $request
     * @return type
     */
    public function getToken(ResponseInterface $response, RequestInterface $request){
        try{
            $userId = AuthenticationMiddleware::getUserId($request);
            $token = $this->_authenticationService->generateToken($userId);
            return $response->withJson($token, 200);
        } catch(UnauthorizedException $ex){
            return $response->withStatus(401);
        }
        catch (Exception $ex) {
            return $response->withStatus(500)->getBody()->write($ex->getMessage());
        }
    }
    
    /**
     * returns decrypted and validated information in case service doesn't want to decrypt the service or doesn't have the public key
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function validateToken(ResponseInterface $response, RequestInterface $request){
        try{
            $token = BearerMiddleware::getToken($request);
            $result = $this->_authenticationService->validateToken($token);
            return $response->withJson($result, 200);
        }catch(UnauthorizedException $ex){
            return $response->withStatus(401);
        } 
        catch (Exception $ex) {
           return $response->withStatus(500);
        }
        
    }
    
    public function refresh(ResponseInterface $response, RequestInterface $request){
        throw new Exception("Not implemented yet");
    }
}