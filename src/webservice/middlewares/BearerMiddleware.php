<?php

namespace userservice\webservice\middlewares;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Http\ServerRequest;
use Slim\Psr7\Response;
use userservice\core\exceptions\UnauthorizedException;
use userservice\core\services\AuthenticationServiceInterface;

class BearerMiddleware{
    private const AUTHORIZE_TOKEN_REGEX = '/^(?<scheme>(Bearer)) (?<token>.*)$/';
    private const TOKEN_KEY = "token";
    private const SCHEME_KEY ="scheme";
    private const BEARER = "Bearer";
    /**
     *
     * @var AuthenticationServiceInterface
     */
    private $_authenticationService;
    
    public function __construct(AuthenticationServiceInterface $authenticationService) {
        $this->_authenticationService = $authenticationService;
    }
    
    public function __invoke(ServerRequest $request, RequestHandlerInterface $handler) {
        try{
            $authorizationHeader = $request->getHeader("Authorization");
            if(empty($authorizationHeader) || count($authorizationHeader) == 0){
                throw new UnauthorizedException();
            }
            $match = preg_match(self::AUTHORIZE_TOKEN_REGEX,$authorizationHeader[0] , $str);
            if(empty($authorizationHeader) || !$match){
                throw new UnauthorizedException();
            }
            $token = $str[self::TOKEN_KEY];
            $scheme = $str[self::SCHEME_KEY];
            if(empty($scheme) || $scheme != self::BEARER){
                throw new UnauthorizedException();
            }
            $request = $request->withAttribute(self::TOKEN_KEY, $token);
            $response = $handler->handle($request);
            return $response;
        } catch (Exception $ex) {
            return new Response(400);
        }
    }
    
    public static function getToken(RequestInterface $request){
        return $request->getAttribute(self::TOKEN_KEY);
    }
}