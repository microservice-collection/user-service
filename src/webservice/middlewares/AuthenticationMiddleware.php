<?php

namespace userservice\webservice\middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Http\ServerRequest;
use Slim\Psr7\Response;
use userservice\core\services\UserServiceInterface;

//https://www.slimframework.com/docs/v4/concepts/middleware.html
class AuthenticationMiddleware{
    private const AUTHORIZATION_HEADER = "Authorization";
    private const AUTHORIZE_TOKEN_REGEX = '/^(?<scheme>(Bearer|Basic)) (?<token>.*)$/';
    private const AUTHORIZE_TOKEN_SCHEME_KEY = "scheme";
    private const AUTHORIZE_TOKEN_TOKEN_KEY = "token";
    
    private const SCHEME_KEY = "scheme";
    private const USER_ID_KEY = "user-id";
    private const PASSWORD_KEY = "password";
    private const TOKEN_KEY = "token";
    
    private const SCHEME_BASIC = "Basic";
    private const SCHEME_BEARER = "Bearer";
    
    private $_userService;
   
    public function __construct(UserServiceInterface $userService) {
        $this->_userService = $userService;
    }
    
    
     /**
     * middleware invokable class
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequest $request, RequestHandlerInterface $handler) {
        try{
            $authority =$request->getUri()->getUserInfo(); 
            if(!empty($authority)){
                $credentials = explode(":", $authority);
                $user = $this->_userService->authenticate($credentials[0],$credentials[1]);
                $request = $request->withAttribute(self::USER_ID_KEY, $user->getId());
            }
            $response = $handler->handle($request);
            return $response;
        } catch (Exception $ex) {
            return new Response(400);
        }
        
    }
    
    /**
     * extracts user id from request header
     * @param ServerRequest $request
     * @return string|null
     */
    public static function getUserId(ServerRequest $request): ?string{
        return $request->getAttribute(self::USER_ID_KEY);
    }
}