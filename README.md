# User-Service

Microservice for handling users.

## Installation

Start the environment:

```
 docker-compose -f docker-compose.dev.yml up -d
```

Clear Environment with deleting volumes:(**Attention**)

```
docker-compose down -v
```

## Usage

Run php server from user-service root directory:

```
php -S localhost:80 -t src
```

## Migrations & Seeding

To migrate and seed the database run the following commands ([link](http://docs.phinx.org/en/latest/commands.html#the-seed-run-command)):

```
//hepl
vendor/bin/phinx
vendor/bin/phinx create migration MyMigration
vendor/bin/phinx migrate -e development
vendor/bin/phinx seed: run -e development
phinx seed:create MyNewSeeder
phinx seed:run -e development -s MyNewSeeder
```

## Configuration

The service can be configured in config/config.json.

```Json
{
    "authenticationType": 'password',
    "jwtPrivateKeyPath": 'path-to-key-file',
    "jwtPublicKeyPath": 'path-to-key-file,
    "dsn": "mysql:host=localhost;dbname=user-service",
    "username": "root",
    "passwd: "pw
}
```

|Key|Example|Description|
|---|---|---|
|**authenticationType**|```password```|Defines the authentication-type by following options: ```password```, ```ldap```, ```email```|
|**jwtPrivateKeyPath**|```../config/myKey.key```|The private Key is needed to generate the RS256-encoded JWT-Token.|
|**jwtPublicKeyPath**|```../config/myKey.key.pub```|This public key is needed to decode the RS256-encoded JWT-Token to authenticate the user. The public key file can be shared within other services like eg. the ```api-gateway```.|
|**dsn**|```mysql:host=localhost;dbname=user-service```|Connection string to database|
|**username**|```root```|Database username. It's recomended to give each micro-service an dedicated user. Also if you use only on database, the respective user should have only access to his own tables.|
|**passwd**|```pw```|Database-User password|

### AuthenticationType

The service provides the following authentication-types:

|Authentication-Type|Description|
|---|---|
|**database**|The user needs to provide username and password to get authorized. Precondition is, that this user with password is created in database. |
|**ladp**|The user needs to provide username and password to get authorized via LDAP-Service. With LDAP all users and roles/groups are maintaind within LDAP so that the user routes like create, update, getIndex and get con't be used anymore.|
|**email**|To get authorized the user has to request an email including an authorization token. This token is also stored in database. Clicking the link in the email the user gets authorized|

## Routes

|Route|Method|Description|Hint|
|---|---|---|---|
|**/token**|```POST```|Authorizes user depending on the configurated ```authorization-type``` and returns RS256-encoded token||
|**/token**|```GET```|Decodes respectively validates token that has to be provided within Http-Request-Header ```Authorization``` as ```Bearer```||
|**/users**|```GET```|Returns list of users. The list can be filtered by optional parameters|Doesn't work for ```Authentication-Type``` ldap where the user administration is taken over by Active-Directory |
|**/users**|```POST```|Creates new User in database|Doesn't work for ```Authentication-Type``` ldap where the user administration is taken over by Active-Directory |
|**/users/{id}**|```GET```|Gets single user by id|Doesn't work for ```Authentication-Type``` ldap where the user administration is taken over by Active-Directory |
|**/users/{id}**|```DELETE```|Deletes user from database | Doesn't work for ```Authentication-Type``` ldap where the user administration is taken over by Active-Directory. User is not deleted in other services |


The Routes can/should be redirected/overwritten by api-gateway.

## Unit Tests

From root directory run following command to execute tests:

```shell
./vendor/bin/phpunit tests --testdox
```

## References

|Reference|Link|
|---|---|
|**firebase/php-jwt**|[Link](https://github.com/firebase/php-jwt)|


## Tipps

### Generate RSA256-Key

```sh
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
```