<?php

use PHPUnit\Framework\TestCase;

use userservice\core\repositories\UserRepositoryInterface;
use userservice\core\services\ConfigurationServiceInterface;
use userservice\infrastructure\services\UserService;
use userservice\core\models\User;
use userservice\core\exceptions\NotFoundException;

class UserServiceTest extends TestCase{
    
    /**
     * @test
     * @return void
     */
    public function createUserSucceeds() : void{
        //Prepare
        $configurationServiceMock = $this->getMockBuilder(ConfigurationServiceInterface::class)->setMockClassName('ConfigurationService')->getMock();
        
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->setMockClassName('UserRepository')->getMock();
        $user = new User();
        $user->setEmail('max.mustermann@test.de');
        $user->setId('some-id');
        $user->setName('Max');
        $user->setPassword('blaa');
        
        $userService = new UserService($userRepositoryMock, $configurationServiceMock);
        
        //ACT
        $user = $userService->create($user);
        
        //ASSERT
        $this->assertNotNull($user);
    }
    
    /**
     * @test
     * @return void
     */
    public function getReturnsSingleUser(): void{
        //PREPARE
        $configurationServiceMock = $this->getMockBuilder(ConfigurationServiceInterface::class)->setMockClassName('ConfigurationService')->getMock();
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->setMockClassName('UserRepository')->getMock();
        $userRepositoryMock->method('get')->willReturn(new User());
        $userService = new UserService($userRepositoryMock, $configurationServiceMock);
        
        //ACT
        $user = $userService->get("some-key");
        
        //Assert
        $this->assertNotNull($user);
    }
    
    /**
     * @test
     * @return void
     */
    public function getUserWithoutResultThrowsNotFoundException():void{
         //PREPARE
        $this->expectException(NotFoundException::class);
        $configurationServiceMock = $this->getMockBuilder(ConfigurationServiceInterface::class)->setMockClassName('ConfigurationService')->getMock();
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->setMockClassName('UserRepository')->getMock();
        $userRepositoryMock->method('get')->willReturn(null);
        $userService = new UserService($userRepositoryMock, $configurationServiceMock);
        
        //ACT
        $user = $userService->get("some-key");
    }
    
    /**
     * @test
     * @return void
     */
    public function getIndexWithoutResultThrowsNotFoundException(): void{
        //PREPARE
        $this->expectException(NotFoundException::class);
        $configurationServiceMock = $this->getMockBuilder(ConfigurationServiceInterface::class)->setMockClassName('ConfigurationService')->getMock();
        $userRepositoryMock = $this->getMockBuilder(UserRepositoryInterface::class)->setMockClassName('UserRepository')->getMock();
        $userRepositoryMock->method('getIndex')->willReturn(null);
        $userService = new UserService($userRepositoryMock, $configurationServiceMock);
        
        //ACT
        $user = $userService->getIndex();
    }
}