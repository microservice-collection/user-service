<?php

use PHPUnit\Framework\TestCase;

use userservice\core\models\User;
use userservice\core\exceptions\ValidationException;

class UserTest extends TestCase{
    /**
     * @test
     * @return void
     */
    public function someTest() : void{
        //Prepare
        $this->expectException(ValidationException::class);
        $this->expectExceptionMessage('The email address has wrong format');
        $user = new User();
        $user->setEmail("bla.bla.com");
        
        //Act 
        $user->validate();
    }
}