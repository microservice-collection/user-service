<?php

use Phinx\Migration\AbstractMigration;

class CreateUserRolesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
            $userRoles = $this->table('user_role', array('id'=> false, 'primary_key' => array('id')));
            $userRoles
                    ->addColumn('id', 'string', array('limit' => 36, 'null' => false))
                    ->addColumn('role_id', 'string', array('limit' => 36, 'null' => false))
                    ->addColumn('user_id', 'string', array('limit' => 36, 'null' => false))
                    ->addIndex(array('id'), array('unique' => true))
                    ->addIndex(array('user_id', 'role_id'), array('unique' => true))
                    ->addTimestamps('created_at', 'deleted_at')
                    ->addForeignKey('user_id', 'users', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
                    ->addForeignKey('role_id', 'roles', 'id', array('delete' => 'NO_ACTION', 'update' => 'NO_ACTION'))
                    ->create();
    }
}
