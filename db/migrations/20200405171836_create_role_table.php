<?php

use Phinx\Migration\AbstractMigration;

class CreateRoleTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $roles = $this->table('role', array('id' => false, 'primary_key' => array('id')));
        $roles
            ->addColumn('id', 'string', array('limit' => 36, 'null' => false))
            ->addColumn('name', 'string', array('limit' => 100, 'null' => false))
            ->addTimestamps('created_at', 'deleted_at')
            ->addIndex(array('id'), array('unique' => true))
            ->Create();
    }
}
