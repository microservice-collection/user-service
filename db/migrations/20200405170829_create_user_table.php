<?php

use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $users = $this->table('user', array('id' => false, 'primar_key' => array('id')));
        $users
            ->addColumn('id', 'string', array('limit' => 36, 'null' => false))
            ->addColumn('email', 'string', array('limit' => 100, 'null' => false))
            ->addColumn('name', 'string', array('limit' => 50, 'null' => true))
            ->addColumn('last_name', 'string', array('limit' => 70, 'null' => true))
            ->addColumn('last_login', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('birthday', 'datetime', array('null' => true))
            ->addColumn('password', 'string', array('limit' => 50, 'null' => false))
            ->addTimestamps('created_at', 'deleted_at')
            ->addIndex(array('id'), array('unique' => true))
            ->addIndex(array('email'), array('unique' => true))
            ->Create();

    }
}
