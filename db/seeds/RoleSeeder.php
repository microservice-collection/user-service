<?php


use Phinx\Seed\AbstractSeed;
use Ramsey\Uuid\Uuid;

class RoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        //'id' => Uuid::uuid4()->toString(),
        $data = [
            [
                'id' => 'ac36fb78-e862-418a-b637-95f908e2e55d',
                'name' => 'User'
            ],
            [
                'id' => 'f74a621a-c4fc-4362-ac9a-6667989ff42b',
                'name' => 'Admin'
            ]
        ];
        $roles = $this->table('role');
        $roles->insert($data)->save();
    }
}
