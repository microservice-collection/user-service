<?php


use Phinx\Seed\AbstractSeed;
use Ramsey\Uuid\Uuid;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
         $data = [
            [
                'id' => '09ad4140-34ba-423a-8f94-811750c3eff5',
                'name' => 'Sascha',
                'last_name'=> 'Franke',
                'email' => 'info@sascha-franke.com',
                'birthday' => '1989-09-06',
                'password' =>'password1'
            ],
            [
                'id' => '4807ad13-3a0c-4c2b-aa87-0d1bb1c41596',
                'name' => 'Helga',
                'last_name' => 'Mustrfrau',
                'email' => 'helga-musterfrau@gmail.com',
                'birthday' => '1900-01-01',
                'password' =>'password2'
            ],
            [
                'id' => 'fe1bac49-d9ba-4515-97bf-34c94dda3243',
                'name' => 'Max',
                'email' => 'max@mustermann.de',
                'birthday' => '1922-04-07',
                'password' =>'password3'
            ]
        ];
        
        $users = $this->table('user');
        $users
            ->insert($data)
            ->save();

    }
}
