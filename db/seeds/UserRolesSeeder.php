<?php


use Phinx\Seed\AbstractSeed;

class UserRolesSeeder extends AbstractSeed
{
    
    public function getDependencies()
    {
        return [
            'UserSeeder',
            'RoleSeeder'
        ];
    }
    
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data =[
            [
                'id' => '11632dee-424b-4a71-9d1d-e52701fab8b8',
                'role_id' => 'ac36fb78-e862-418a-b637-95f908e2e55d',
                'user_id' =>  '09ad4140-34ba-423a-8f94-811750c3eff5'
            ],
            [
                'id' => '4919de97-674a-43d0-b9b6-4235a2237581',
                'role_id' => 'ac36fb78-e862-418a-b637-95f908e2e55d',
                'user_id' =>  '4807ad13-3a0c-4c2b-aa87-0d1bb1c41596'
            ],
            [
                'id' => 'be1e5a3e-dc75-4038-bd30-bd3d3f757338',
                'role_id' => 'ac36fb78-e862-418a-b637-95f908e2e55d',
                'user_id' =>  'fe1bac49-d9ba-4515-97bf-34c94dda3243'
            ],
            [
                'id' => '740f0233-432c-4655-b3c1-17b7ab59afea',
                'role_id' => 'f74a621a-c4fc-4362-ac9a-6667989ff42b',
                'user_id' =>  '09ad4140-34ba-423a-8f94-811750c3eff5'
            ]
        ];
        $userRoles = $this->table('user_role');
        $userRoles
            ->insert($data)
            ->save();
    }
}
