<?php

return [
    'settings.responseChunkSize' => 4096,
    'settings.outputBuffering' => 'append',
    'settings.determineRouteBeforeAppMiddleware' => false,
    'settings.displayErrorDetails' => true,
    'settings.db' => [
        'driver' => 'localhost',
        'host' => '',
        'database' => 'user-service',
        'user' => 'root',
        'password' => 'pw'
    ],
];